
   <div class="row">
      <div class="large-12 columns">
<ul class="example-orbit" data-orbit>
  <li>
    <img src="img/cover_salty_01.jpg" alt="slide 1" />
    <div class="orbit-caption">
      Caption One.
    </div>
  </li>
  
</ul>
</div>
</div>
<div class="row">
  <div class="small-2 medium-2 large-2 columns">
    <img src="img/salty/Chicken_curry_pie_200x200.jpg" class="sweetypic" />
  </div>
  <div class="small-10 medium-10 large-10 columns saltyitemtext">
   <div class="porducttextchickencurrypie saltyitem">
    <div class="clearfix">
    <div class="right" style="width: 174px;">
    <span class="saltyingredients">INFORMATIONS:<br /><br />
    <!--Traditional Australian Recipe<br />Serving per package: 1<br />Serving size: 200g NET</span><br /><br />-->Serving per package 1<br />
Serving size 200g<br /><br /><span class="label">2200 MMK</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png" class="plusquantity">
    
    <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
    <input type="hidden" name="my-item-id" value="SCC1" />
    <input type="hidden" name="my-item-name" value="Chicken Curry Pie" />
    <input type="hidden" name="my-item-price" value="2200" />
    <input type="submit" name="my-add-button" value="ADD" />
    </form>
    </div>
    <div class="left" style="width: 60%;">
   
      <span class="productsalty">Chicken Curry Pie<br /></span><br /><span class="saltyproducttext846">Flour, baking powder, milk powder, vegetable shortening, chicken meat, onion, pie gel (thickener), potato, curry powder, salt, sugar, pepper and garlic.</span>
    
    </div>
    
    </div>
    </div>
  </div>
  

</div>

<div class="row">
  <div class="small-2 medium-2 large-2 columns">
    <img src="img/salty/Chicken_vegetable_pie_200x200.jpg" class="sweetypic" />
  </div>
  <div class="small-10 medium-10 large-10 columns saltyitemtext">
   <div class="producttextchickenbegetablepie saltyitem">
    <div class="clearfix">
    <div class="right" style="width: 174px;">
    <span class="saltyingredients">INFORMATIONS:<br /><br />
   Serving per package 1<br />
Serving size 200g<br /><br /><span class="label">2200 MMK</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png" class="plusquantity">
     <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
    <input type="hidden" name="my-item-id" value="SCC2" />
    <input type="hidden" name="my-item-name" value="Chicken Vegetable Pie" />
    <input type="hidden" name="my-item-price" value="2200" />
    <input type="submit" name="my-add-button" value="ADD" />
    </form>
    </div>
    <div class="left" style="width: 60%;">
   
     <span class="productsalty">Chicken Vegetable Pie<br /> </span><br /><span class="saltyproducttext846">Flour, vegetable shortening, salt, baking powder, milk powder, chunky beef steak, onion, pie gel (thickener), water, sugar, pepper, potato, curry powder and garlic.
</span>
    
    </div>
    
    </div>
    </div>
  </div>
  
  <!--<div class="large-6 columns">
  <div class="porducttext">
      <span class="producttext846">NEW </span><span class="producttextlobster">Chocolate Chips Cookies </span><span class="producttext846">MORE CHOCOLATE PIECES WITH A GENEROUS MILK</span>
    </div>
  </div>-->
</div>
<div class="row">
  <div class="small-2 medium-2 large-2 columns">
    <img src="img/salty/Beef_Curry_pie_200x200.jpg" class="sweetypic" />
  </div>
  <div class="small-10 medium-10 large-10 columns saltyitemtext">
   <div class="producttextbeefcurrypie saltyitem">
    <div class="clearfix">
    <div class="right" style="width: 174px;">
    <span class="saltyingredients">INFORMATIONS:<br /><br />
   Serving per package 1<br />
Serving size 200g<br /><br /><span class="label">2200 MMK</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png" class="plusquantity">
     <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
    <input type="hidden" name="my-item-id" value="SCC3" />
    <input type="hidden" name="my-item-name" value="Beef Curry Pie" />
    <input type="hidden" name="my-item-price" value="2200" />
    <input type="submit" name="my-add-button" value="ADD" />
    </form>
    </div>
    <div class="left" style="width: 60%;">
   
     <span class="productsalty">Beef Curry Pie<br /> </span><br /><span class="saltyproducttext846">Flour, vegetable shortening, salt, baking powder, milk powder, chicken meat, onion, pie gel (thickener), water potato, sugar, pepper, potato, curry powder and garlic.</span>
    
    </div>
    
    </div>
    </div>
  </div>
  
  <!--<div class="large-6 columns">
  <div class="porducttext">
      <span class="producttext846">NEW </span><span class="producttextlobster">Chocolate Chips Cookies </span><span class="producttext846">MORE CHOCOLATE PIECES WITH A GENEROUS MILK</span>
    </div>
  </div>-->
</div>
<div class="row">
  <div class="small-2 medium-2 large-2 columns">
    <img src="img/salty/Chunky_Beef_Steak_Pie_200x200.jpg" class="sweetypic" />
  </div>
  <div class="small-10 medium-10 large-10 columns saltyitemtext">
   <div class="producttextchunkysteakbeefpie saltyitem">
    <div class="clearfix">
    <div class="right" style="width: 174px;">
    <span class="saltyingredients">INFORMATIONS:<br /><br />
    Serving per package 1<br />
Serving size 200g<br /><br /><span class="label">2200 MMK</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png" class="plusquantity">
     <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
    <input type="hidden" name="my-item-id" value="SCC4" />
    <input type="hidden" name="my-item-name" value="Chunky Steak Beef Pie" />
    <input type="hidden" name="my-item-price" value="2200" />
    <input type="submit" name="my-add-button" value="ADD" />
    </form>
    </div>
    <div class="left" style="width: 60%;">
   
      <span class="productsalty">Chunky Steak Beef Pie<br /> </span><br /><span class="saltyproducttext846">Flour, vegetable shortening, salt, baking powder, milk powder, chunky beef steak, onion, pie gel (thickener), vegetable (corn, carrot, green bean) water, sugar, pepper and garlic.</span>
    
    </div>
    
    </div>
    </div>
  </div>
  
  <!--<div class="large-6 columns">
  <div class="porducttext">
      <span class="producttext846">NEW </span><span class="producttextlobster">Chocolate Chips Cookies </span><span class="producttext846">MORE CHOCOLATE PIECES WITH A GENEROUS MILK</span>
    </div>
  </div>-->
</div>

<div class="row">
  <div class="small-2 medium-2 large-2 columns">
    <img src="img/salty/Chicken_pies_200x200.jpg" class="sweetypic" />
  </div>
  <div class="small-10 medium-10 large-10 columns saltyitemtext">
   <div class="producttextchickenpie saltyitem">
    <div class="clearfix">
    <div class="right" style="width: 174px;">
    <span class="saltyingredients">INFORMATIONS:<br /><br />
    Serving per package 2<br />
Serving size 200g<br /><br /><span class="label">2200 MMK</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png" class="plusquantity">
     <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
    <input type="hidden" name="my-item-id" value="SCC5" />
    <input type="hidden" name="my-item-name" value="Chicken Pies" />
    <input type="hidden" name="my-item-price" value="2200" />
    <input type="submit" name="my-add-button" value="ADD" />
    </form>
    </div>
    <div class="left" style="width: 60%;">
   
     <span class="productsalty">Chicken Pies<br /> </span><br /><span class="saltyproducttext846">Flour, vegetable shortening, salt, baking powder, milk powder, chunky beef steak, onion, pie gel (thickener), water, sugar, pepper, potato, curry powder and garlic.</span>
    
    </div>
    
    </div>
    </div>
  </div>
  </div>
<div class="row">
  <div class="small-2 medium-2 large-2 columns">
    <img src="img/salty/Beef_Pies_200x200.jpg" class="sweetypic" />
  </div>
  <div class="small-10 medium-10 large-10 columns saltyitemtext">
   <div class="producttextbeefpie saltyitem">
    <div class="clearfix">
    <div class="right" style="width: 174px;">
    <span class="saltyingredients">INFORMATIONS:<br /><br />
   Serving per package 2<br />
Serving size 200g<br /><br /><span class="label">2200 MMK</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png" class="plusquantity">
     <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
    <input type="hidden" name="my-item-id" value="SCC6" />
    <input type="hidden" name="my-item-name" value="Beef Pies" />
    <input type="hidden" name="my-item-price" value="2200" />
    <input type="submit" name="my-add-button" value="ADD" />
    </form>
    </div>
    <div class="left" style="width: 60%;">
   
     <span class="productsalty">Beef Pies<br /> </span><br /><span class="saltyproducttext846">Flour, vegetable shortening, salt, baking powder, milk powder, chunky beef steak, onion, pie gel (thickener), potato, curry powder, water, sugar, pepper and garlic.</span>
    
    </div>
    
    </div>
    </div>
  </div>
  

  <!--<div class="large-6 columns">
  <div class="porducttext">
      <span class="producttext846">NEW </span><span class="producttextlobster">Chocolate Chips Cookies </span><span class="producttext846">MORE CHOCOLATE PIECES WITH A GENEROUS MILK</span>
    </div>
  </div>-->
</div>
<div class="row">
  <div class="small-2 medium-2 large-2 columns">
    <img src="img/salty/Chicken_Pastry_Roll_200x200.jpg" class="sweetypic" />
  </div>
  <div class="small-10 medium-10 large-10 columns saltyitemtext">
   <div class="producttextchickenpastryroll saltyitem">
    <div class="clearfix">
    <div class="right" style="width: 174px;">
    <span class="saltyingredients">INFORMATIONS:<br /><br />
    Serving per package 4<br />
Serving size 180g<br /><br /><span class="label">1500 MMK</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png" class="plusquantity">
     <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
    <input type="hidden" name="my-item-id" value="SCC7" />
    <input type="hidden" name="my-item-name" value="Chicken Pastry Roll" />
    <input type="hidden" name="my-item-price" value="1500" />
    <input type="submit" name="my-add-button" value="ADD" />
    </form></div>
    <div class="left" style="width: 60%;">
   
      <span class="productsalty">Chicken Pastry Roll<br /> </span><br /><span class="saltyproducttext846">Mince chicken, flour, bread crumb, vegetable shortening, onion, salt, pepper and sugar.</span>
    
    </div>
    
    </div>
    </div>
  </div>
  
  <!--<div class="large-6 columns">
  <div class="porducttext">
      <span class="producttext846">NEW </span><span class="producttextlobster">Chocolate Chips Cookies </span><span class="producttext846">MORE CHOCOLATE PIECES WITH A GENEROUS MILK</span>
    </div>
  </div>-->
</div>
<div class="row">
  <div class="small-2 medium-2 large-2 columns">
    <img src="img/salty/Beef_Pastry_Roll_200x200.jpg" class="sweetypic" />
  </div>
  <div class="small-10 medium-10 large-10 columns saltyitemtext">
   <div class="producttextbeefpastryroll saltyitem">
    <div class="clearfix">
    <div class="right" style="width: 174px;">
    <span class="saltyingredients">INFORMATIONS:<br /><br />
   Serving per package 4<br />
Serving size 180g<br /><br /><span class="label">1500 MMK</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png" class="plusquantity">
     <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
    <input type="hidden" name="my-item-id" value="SCC8" />
    <input type="hidden" name="my-item-name" value="Beef Pastry Roll" />
    <input type="hidden" name="my-item-price" value="1500" />
    <input type="submit" name="my-add-button" value="ADD" />
    </form>
    </div>
    <div class="left" style="width: 60%;">
   
      <span class="productsalty">Beef Pastry Roll<br /> </span><br /><span class="saltyproducttext846">Mince beef, flour, bread crumb, vegetable shortening, onion, salt, pepper and sugar.</span>
    
    </div>
    
    </div>
    </div>
  </div>
  
  <!--<div class="large-6 columns">
  <div class="porducttext">
      <span class="producttext846">NEW </span><span class="producttextlobster">Chocolate Chips Cookies </span><span class="producttext846">MORE CHOCOLATE PIECES WITH A GENEROUS MILK</span>
    </div>
  </div>-->
</div>
<div class="row">
  <div class="small-2 medium-2 large-2 columns">
    <img src="img/salty/Garlic_Bread_200x200.jpg" class="sweetypic" />
  </div>
  <div class="small-10 medium-10 large-10 columns saltyitemtext">
   <div class="producttextgarlicbread saltyitem">
    <div class="clearfix">
    <div class="right" style="width: 174px;">
    <span class="saltyingredients">INFORMATIONS:<br /><br />
    Serving per package 4<br />
Serving size 70g<br /><br /><span class="label">2000 MMK</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png" class="plusquantity">
     <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
    <input type="hidden" name="my-item-id" value="SCC9" />
    <input type="hidden" name="my-item-name" value="Garlic Bread" />
    <input type="hidden" name="my-item-price" value="2000" />
    <input type="submit" name="my-add-button" value="ADD" />
    </form>
    </div>
    <div class="left" style="width: 60%;">
   
      <span class="productsalty">Garlic Bread<br /> </span><br /><span class="saltyproducttext846">Bread flour, vegetable shortening, salt, crushed garlic, herbs, yeast, butter, sunflower oil.</span>
    
    </div>
    
    </div>
    </div>
  </div>
  </div>
 <!-- </div>-->



