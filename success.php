<div class="row">
	<div class="small-12 columns">
		<h4 class="subheader" id='card_header'>Order Successfully Complete!</h4>
		<p class='card_text'>Thank you for your online order with Sydney’s Home Bakery Goods.<br/><br/>
		Our team will contact you as soon as possible by phone or email to finalise the order with you. <br/><br/>

					Thank you for you trust, <br/><br/>

					Sydney’s team</p>
		<p><?php echo $contact_name;?></p>
	</div>
</div>