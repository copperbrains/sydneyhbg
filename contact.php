 
 <script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'clean'
 };
 </script>
 <div class="row">
	<div class="small-12 columns">
		<div id="map" style="width: 100%;height: 450px"></div>
	</div>
</div>
<div class="row">
<div class="small-6 columns contactright">
	<h3>ABOUT THE BRAND</h3>
	<p>
	From Australia to Myanmar (Yangon) here at Sydney's Home Baked Goods we create sweet bites of goodness for our customers. Specialising in custom orders, from cupcakes to special creations, to Sunday brunch sweet delicacies. Sydney's Home Baked Goods is Yangon's premium destination for baked goods.
We distinguish ourselves on the quality and taste of our fresh baked goods. Quality: we use the best ingredients with no preservatives and taste, because we create with flavours stemming from inspiration around the world and secret family recipes. 
	</p>
	<p>
	At Sydney's Home Baked Goods our creation's become a part of our customer’s important moments, from birthdays, weddings, grand openings and family dinners. We continue to be a part of your milestones.
Proud to have introduced the cupcake concept to Myanmar, throughout the years we have lived up to our reputation to serve more of our baked goods to fellow epicureans and to those who simply enjoy a "piece of cake" with their tea. 
</p>
<p>Sydney's Home Baked Goods can be found at premium retail outlets, supermarkets, cafes and at our personal hub, the place that bakes it all.</p>
	</div>
	<div class="small-6 columns contactleft">
	<div class="contactform">
    <form parsley-validate method="post" action="contactemail.php">
      <input type="text" placeholder="First Name, Last Name" id="contactname" name="contactname" parsley-required="true" class="required"/>
      <input type="text" placeholder="Subject" id="contactcompany" name="contactsubject" parsley-required="true" class="required"/>
      <input type="text" placeholder="Email address" id="contactemail" name="contactemail" parsley-required="true" parsley-type="email" parsley-trigger="change" class="required"/>
      <input type="text" placeholder="Phone number" id="contactphone" name="contactphone" parsley-required="true" class="required"/>
      <textarea placeholder="Messages" name="message" parsley-required="true" class="required"></textarea>
	<?php
     		require_once('recaptchalib.php');
     		$publickey = '6LfQQ-4SAAAAAEfd5soV89KF9ff4qdfXJo8hLOsS'; // you got this from the signup page
     		echo recaptcha_get_html($publickey);
   		?>
		<div>
			<?php if($_GET['error']=='captcha'){
				echo 'Re-captcah is failed. Please try again ';
			}
			?>
		</div><br />
      <input type="submit" value="SEND" name="submit" />
    </form>
  </div>
	</div>
	
</div>


