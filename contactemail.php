<?php

	require_once 'lib/swift_required.php'; ?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sydney's Home Baked Goods</title>
   <!-- <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/custom.css" />-->

    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.min.css" />
    <link rel="stylesheet" href="css/style.css" />
     <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
     <link rel="stylesheet" type="text/css" href="css/component.min.css" />
     <link rel="stylesheet" type="text/css" href="css/custom.css" />
    <script src="js/modernizr.js"></script>


  </head>
  <body>
 
<div class="container">



  <div class="show-for-medium-up">
<?php include('menu.php'); ?>
</div>
<div class="show-for-small-only">
<?php include('mobilemenu.php'); ?>
</div>




  <div class="row">
      <div class="small-12 columns">
<?php
function check_input($data, $problem='')
  {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    if ($problem && strlen($data) == 0)
    {
      show_error($problem);
    }
    return $data;
  }

function redirect($url){
    if (headers_sent()){
      die('<script type="text/javascript">window.location.href="' . $url . '";</script>');
    }else{
      header('Location: ' . $url);
      die();
    }    
}

 // $url should be an absolute url

	
	if(isset($_POST['submit'])){
	$url = 'http://sydneyshbg.com/index.php?q=contact&error=captcha';

		require_once('recaptchalib.php');
 		$privatekey = "6LfQQ-4SAAAAAK6TKOU99rWAmWr1pOwlF6U-r8Qa";
 $resp = recaptcha_check_answer ($privatekey,
                                 $_SERVER["REMOTE_ADDR"],
                                 $_POST["recaptcha_challenge_field"],
                                 $_POST["recaptcha_response_field"]);
 if (!$resp->is_valid) {
   // What happens when the CAPTCHA was entered incorrectly
  	redirect($url);
	 die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
        "(reCAPTCHA said: " . $resp->error . ")");
	
 } else {
   // Your code here to handle a successful verification
	
		
		
		
		$contact_name = check_input($_POST['contactname']);
		$contact_subject = check_input($_POST['contactsubject']);
		$contact_email = check_input($_POST['contactemail']);
		$contact_phone = check_input($_POST['contactphone']);
		$contact_message = check_input($_POST['message']);
		$contact_body= "Name = $contact_name\nEmail = $contact_email\nPhone Number = $contact_phone\n\n$contact_message";

    $custom_message = "Dear " . $contact_name . ", <br><br>
We confirm that your request has been sent.<br><br>
A member of our team will contact you as soon as possible.<br><br>

Thank you for your trust,<br><br>

The Sydney’s Home Baked Goods team";
		// Create the mail transport configuration
		$transport = Swift_MailTransport::newInstance();
		 
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo(array(
		 'sydneyshbg@gmail.com' => "Sydneys HBG Contact Message"
		));
		$message->setSubject($contact_subject);
		$message->setBody($contact_body);
		//$message->setFrom($contact_email, $contact_name);
    $message->setFrom(array(
     'sydneyshbg.web@gmail.com' => "Sydneys HBG"
    ));
		 
		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		$mailer->send($message);

		echo "<div class='ema'><h4>Your email is successfully sent!</h4></div>";


/*Creating Contact form reply*/
    // Create the mail transport configuration
    $transport = Swift_MailTransport::newInstance();
     
    // Create the message
    $message = Swift_Message::newInstance();
    $message->setTo($contact_email);
    $message->setSubject($contact_subject);
    $message->setBody($custom_message, 'text/html');
    //$message->setFrom($contact_email, $contact_name);
      $message->setFrom(array(
     'sydneyshbg.web@gmail.com' => "Sydneys HBG"
    ));
    // Send the email
    $mailer = Swift_Mailer::newInstance($transport);
    $mailer->send($message);
 }

		

	}
?>
</div>
</div>
<div class="row">
  <div class="large-12 columns">
    <footer>
    <div class="right footermenudiv">
       <ul class="inline-list text-right footermenu">
        <li><a href="index.php">HOME</a></li>
        <li><a href="index.php?q=cupcakes">CUPCAKES</a></li>
        <li><a href="index.php?q=sweety">SWEETY</a></li>
        <li><a href="index.php?q=salty">SALTY</a></li>
        <li><a href="events.php">EVENTS</a></li>
        <li><a href="index.php?q=contact">CONTACT</a></li>
        <li><a href="index.php?q=legal">LEGALS</a></li>
       <li><a href="index.php?q=credit">CREDITS</a></li>
        <li><a href="index.php?q=condition">CONDITIONS</a></li>
       </ul>
       <span class="text-right copyright"> &copy; Copyright- www.sydneyshbg.com - Proplused by <a href="http://synapseoriginal.com" target="_blank">Synapse Original</a>
    </span>
    </div>
    </footer>
  </div>
</div>

 <script src="js/foundation.min.js"></script>
    <script src="js/foundation/foundation.orbit.js"></script>
    <script type="text/javascript">
      $(document).foundation();
    </script>
  </div>
    </body>
</html>
