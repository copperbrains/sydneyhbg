<div class="row">
	<div class="small-12 columns" id='terms'>
		<h3>Terms of use</h3>
		<h3>23.01.2015</h3>
		<p id='legal'>
			
			The www.sydneyshbg.com website is edited by Sydney’s Home Baked Goods herein herein after
also referred to as SHBG. The headquarters are located at MWEA Tower, Shwedagon Pagoda
Road, Yangon, Myanmar.<br><br>
The site administrator is : Sydney’s Home Baked Goods
The site is hosted by ovh.com.<br><br>
Access to the Site and using of its contents is subject to the conditions described below. By
accessing and browsing the Site constitutes part of the user an unconditional acceptance of the
following terms:<br><br>
The site is the exclusive property of SHBG that own exclusive right to use and exploit the
intellectual property rights and personality rights attached to it, including trademarks, designs,
copyright and right to image as the originator or by the effect of an express authorization, patent
and all rights to intellectual property.<br><br>
The use of all or part of the site, including the downloading, reproduction, transmission or
representation for purposes other than private and personal use for non-profit and commercial use
is strictly prohibited unless expressly authorized by the brand. The Site and the SHBG brand is
protected by copyright laws, international treaties and Myanmar laws.<br><br>
SHBG strives to provide the best of its abilities the accuracy and updating of the information
broadcast on the Site, and reserves the right to change at any time and without notice, the
content .<br><br>
As permitted by law, SHBG is not liable :<br><br>
- For any inaccuracy, omission or for any damages resulting from the intrusion of a third party led
to a modification of the information available on the Site.<br><br>
- In case of damage caused as a result of unauthorized access to the Site or impossibility to reach
third.<br><br>
The visitor is informed that access to the Site may be interrupted at any time by SHBG for reasons
of maintenance, security or other technical constraints.<br><br>
To exercise his right of opposition, rectification and correction, the visitor is invited to contact:
contact@synapseoriginal.com<br><br>
SHBG offers users free and unrestricted access all articles published on the site.<br><br>
The User agrees to use the services provided by the "Site" in accordance with the laws, public
order and morality according to the provisions of these Terms of use .<br><br>
The User shall not use the service for illegal purposes or contrary to what is provided for in the
Terms of Use and capable of adversely affecting the rights or interests of third parties or which , in
one way or another might affect the users , services, the "Site" , its image or impede the proper
functioning of services.<br><br>
SHBG asks users to users of the Site do not send pictures, drawings or ideas if the brand has
expressly asked to do so. To avoid any dispute relating to copyright and intellectual property, the
brand does not take into account any such receipt.<br><br>
SHBG also informs visitors that their data will be stored at the host site synapseoriginal.com
located in France and therefore it must by legislation keep all connections to different servers it for
a duration defined by the latter .<br><br>

SHBG may send "cookies" to your computer or the connection used by the user at the time of
connection. These " Cookies" are files that allow easy navigation of the visitor during the session,
and will be required as part of the purchase assignment within the e-commerce site sections. It is
recalled that the visitor may oppose the registration of these "cookies" by configuring their browser
in this direction.<br><br>
SHBG implements technical and organizational measures to ensure the security and confidentiality
of the files generated from the personal data collected on the site. However, SHBG does not
control all the risks associated with the Internet and draws the attention of users of the existence of
potential risks inherent in the use of the Internet.<br><br>
The creation of hypertext links to the Site may be made without the prior written permission of
SHBG which can be revoked at any time. SHBG assumes no responsibility for the content of the
linked website .<br><br>
SHBG informs visitors to the Site that these conditions may be modified at any time. These
changes are published are online and are deemed accepted without reservation by any visitor after
they are posted . It is up to any visitor before browsing the site to read the conditions carefully .
These conditions are governed by Myanmar law. The Myanmar courts will exercise jurisdiction
over any dispute relating to the use of the Site.<br><br>
© 2015
		</p>
	</div>
</div>