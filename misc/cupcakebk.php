<?php 
include_once('jcart/jcart.php');


 ?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sydney's Home Baked Goods</title>
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/modernizr.js"></script>
  </head>
  <body>
    <div class="show-for-medium-up">
    <div class="row">
      <div class="large-12 columns">
        <div class="clearfix custop">
         
    <a class="right"><img src="img/icon_linkedin_sydneys.jpg"></a>
    <a class="right"><img src="img/icon_twitter_sydneys.jpg"></a>
    <a class="right"><img src="img/icon_facebook_sydneys.jpg"></a>
     <a class="right"><span>Your order</span><img src="img/icon_basket_sydneys.jpg"></a>

    </div>
      </div>
    </div>
    <div class="row">
      <div class="small-12 columns">
    <nav class="top-bar" data-topbar>
  <ul class="title-area">
    <li class="name">
      <a href="#"><img src="img/logo.png" class="logo"></a>
    </li>
    <li class="toggle-topbar menu-icon"><a href="#"><span>&nbsp;</span></a></li>
  </ul>

  <section class="top-bar-section">

    <!-- Left Nav Section -->
    <ul class="right">
      <li class="active"><a href="index.html"><img src="img/icon_tab_home.png" class="show-for-medium-up menutop">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HOME</a></li>
      <li><a href="#"><img src="img/icon_tab_cupcakes.png" class="show-for-medium-up menutop">&nbsp;&nbsp;CUPCAKES</a></li>
      <li><a href="sweety.php"><img src="img/icon_tab_sweety.png" class="show-for-medium-up menutop">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SWEETY</a></li>
      <li><a href="#"><img src="img/icon_tab_salty.png" class="show-for-medium-up menutop">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SALTY</a></li>
      <li><a href="#"><img src="img/icon_tab_about_us.png" class="show-for-medium-up menutop">&nbsp;&nbsp;&nbsp;ABOUT US</a></li>
      <li><a href="#"><img src="img/icon_tab_contact.png" class="show-for-medium-up menutop">&nbsp;&nbsp;&nbsp;CONTACT</a></li>
    </ul>
  </section>
</nav>
</div>
</div>
   <div class="row">
    <div class="large-12 columns">
<ul class="example-orbit" data-orbit>
  <li>
    <img src="img/home_slider_img4.jpg" alt="slide 1" />
    <div class="orbit-caption">
      Caption One.
    </div>
  </li>
 
</ul>
</div>
</div> <!--- Slideshow -->
<div class="row">
  <div class="small-6 columns leftcon">
         <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                 
                      <span class="productsalty">Vanilla<br /><br /></span>
                      
                         <span class="cupcakeingredients">INGREDIENTS:<br />
                         Vanilla cake topped with strawberry icing.</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC1" />
                        <input type="hidden" name="my-item-name" value="Vanilla Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                         
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <span class="productsalty">Lime<br /><br /></span>
                      <span class="saltyingredients">INGREDIENTS:<br />Vanilla caked spiked with lime juice and lime zest topped with vanilla icing and candied lime.</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC2" />
                        <input type="hidden" name="my-item-name" value="Lime Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <span class="productsalty">Red Velvet<br /><br /></span>
                      <span class="saltyingredients">INGREDIENTS:<br />
                         Red Velvet cake topped with vanilla icing ans chocolate shavings.</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC3" />
                        <input type="hidden" name="my-item-name" value="Red Velvet Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <span class="productsalty">Rainbow<br /><br /></span>
                      <span class="saltyingredients">INGREDIENTS:<br />Chocolate cake topped with vanilla icing and rainbow sprinkles.</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC4" />
                        <input type="hidden" name="my-item-name" value="Rainbow Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <span class="productsalty">Choc Caramel<br /><br /></span>
                      <span class="saltyingredients">INGREDIENTS:<br />
                         Chocolate cake topped with caramel cream, caramel and sea salt.</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC5" />
                        <input type="hidden" name="my-item-name" value="Choc Caramel Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <span class="productsalty">Choc Ganache<br /><br /></span>
                      <span class="saltyingredients">INGREDIENTS:<br />Chocolate cake topped with rich 
                      chocolate ganache.</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC61" />
                        <input type="hidden" name="my-item-name" value="Choc Ganache Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                     </div>
                 </div>
        </div>
  </div>
       <div class="small-6 columns rightcon">
         <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <span class="productsalty">Chocolate<br /><br /></span>
                      <span class="saltyingredients">INGREDIENTS:<br />Chocolate cake topped with chocolate icing.</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC7" />
                        <input type="hidden" name="my-item-name" value="Chocolate Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <span class="productsalty">Banana<br /><br /></span>
                      <span class="saltyingredients">INGREDIENTS:<br />Banana cake topped with Chocolate icing and banana chip.</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC8" />
                        <input type="hidden" name="my-item-name" value="Banana Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <span class="productsalty">Black Sesame Seed<br /><br /></span>
                      <span class="saltyingredients">INGREDIENTS:<br />Roasted black sesame seed cake topped with black sesame seed icing.</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC9" />
                        <input type="hidden" name="my-item-name" value="Black Sesame Seed Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <span class="productsalty">Carrot<br /><br /></span>
                      <span class="saltyingredients">INGREDIENTS:<br />Carrot cake topped with cream cheese icing (This cake contains walnuts).</span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC10" />
                        <input type="hidden" name="my-item-name" value="Carrot Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <span class="productsalty">Choc Peanut Butter<br /><br /></span>
                      <span class="saltyingredients">INGREDIENTS:<br />Chocolate cake topped with peanut butter
                      cream and chocolate ganache. </span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC11" />
                        <input type="hidden" name="my-item-name" value="Choc Peanut Butter Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-4 medium-4 large-4 columns">
               <img src="img/cupcakes/photo_default_200x200.jpg" class="sweetypic" />
            </div>
            <div class="small-8 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <span class="productsalty">Fruit Passion <br /><br /></span>
                      <span class="saltyingredients">INGREDIENTS:<br />Passion fruit cake spiked with fresh passion
                      fruit juice, passion fruit curd filling topped
                      with passion fruit cream. </span><br /><br /><form method="post" action="" class="jcart"><img src="img/salty/icon_less_000000.png" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/salty/icon_plus_000000.png">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC12" />
                        <input type="hidden" name="my-item-name" value="Fruit Passion Cupcake" />
                         <input type="hidden" name="my-item-price" value="1000" />
                         <input type="submit" name="my-add-button" value="Add to Busket" />
                         </form>
                     </div>
                 </div>
        </div> 
       </div>
</div>

<div class="row">
  <div class="large-12 columns">
    <footer>
       &nbsp;
    </footer>
  </div>
</div>



    <script src="js/jquery.js"></script>
    <script src="jcart/js/jcart.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/foundation/foundation.orbit.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
