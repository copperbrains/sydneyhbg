<div class="row">
  <div class="small-12 columns">
    <ul class="example-orbit" data-orbit>
        <li>
          <a href="index.php?q=customize"><img src="img/cover_home_01.jpg" alt="slide 1" /></a>
          <div class="orbit-caption">
          </div>
        </li>
        <li>
          <a href="index.php?q=customize"><img src="img/cover_home_02.jpg" alt="slide 1" /></a>
          <div class="orbit-caption">
          </div>
        </li>
       <!-- <li>
          <img src="img/home_slider_img2.jpg" alt="slide 2" />
          <div class="orbit-caption">
          </div>
        </li>
        <li>
          <img src="img/home_slider_img3.jpg" alt="slide 3" />
          <div class="orbit-caption">
          </div>
        </li>-->
    </ul>
  </div>
</div><!-- Slideshow -->
<div class="row">
  <div class="small-12 medium-6 large-6 columns columnswithnopaddingright">
  <div class="wheredo">
            <div class"moduletitle" style="padding-top: 20px; padding-left: 20px;">
            <span>WHERE DO YOU FIND US?</span>
          </div>
          <a href="index.php?q=contact" class="shopbutton">FIND A SHOP</a>
   </div>
    <div class="row">
      <div class="small-6 medium-6 large-6 columns journalwidth">
      <div class="journal">
       <div class"moduletitle" style="padding-top: 30px;"><span class="journaltext">WHAT<br />THEY SAY<br />ABOUT<br /> US...</span></div>
       <a href="index.php?q=press" class="pressbutton">READ MORE</a>
      </div>
        
      </div><!-- What they say about us -->
      <div class="small-6 medium-6 large-6 columns presspicmain">
       <a class="th" href="index.php?q=press"><img src="img/press/Press_7.jpg"></a>
      </div><!-- Megazine pictures -->
    </div><!-- megazine row -->
  </div><!-- left side main -->
  <div class="small-12 medium-6 large-6 columns columnswithnopaddingleft">
    <div class="facebooksocial">
        <div class"moduletitle" style="padding-top: 20px; padding-left: 20px;">
          <span class="socialfirst">JOIN THE</span><span class="socialsecond"> SYDNEY'S<br /> COMMUNITY</span><span class="socialfirst"> ON FACEBOOK</span>
        </div>
        <div class="facebookbox">
        <!--<img src="img/facebook_timeline_img.jpg" class="fbpic">-->
        <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fsydneysyangon&amp;width=300&amp;height=558&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=true&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:515px;" allowTransparency="true"></iframe>
        </div>
      </div>
  </div><!-- right side Facebook Page -->
  </div>
   