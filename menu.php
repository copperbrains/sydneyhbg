<div class="row">
      <div class="large-12 columns">
        <div class="clearfix custop">
         
    <a class="right" href="https://www.linkedin.com/company/sydney%27s-home-baked-goods?trk=biz-companies-cym" target="_blank"><img src="img/icon_linkedin_sydneys.jpg"></a>
    <!--<a class="right"><img src="img/icon_twitter_sydneys.jpg"></a>-->
    <a class="right" href="https://www.facebook.com/sydneysyangon" target="_blank"><img src="img/icon_facebook_sydneys.jpg"></a>
    
      </div>
    </div>
    
    </div>
    <div class="row">
      <div class="small-12 columns">
    <nav class="top-bar cl-effect-5" data-topbar>
  <ul class="title-area">
    <li class="name">
      <a href="index.php"><img src="img/logo.png" class="logo"></a>
    </li>
    <li class="toggle-topbar menu-icon"><a href="#"><span>&nbsp;</span></a></li>
  </ul>

  <section class="top-bar-section">

    <!-- Left Nav Section -->
    <ul class="right">
      <li class="active"><a href="index.php"><span data-hover="&nbsp;&nbsp;&nbsp;&nbsp;HOME"><img src="img/icon_tab_home.png" class="show-for-medium-up menutop"></a></li>
      <li><a href="index.php?q=cupcakes"><span data-hover="&nbsp;CUPCAKES"><img src="img/icon_tab_cupcakes.png" class="show-for-medium-up menutop"></span></a></li>
      <li><a href="index.php?q=sweety"><span data-hover="&nbsp;&nbsp;&nbsp;SWEETY"><img src="img/icon_tab_sweety.png" class="show-for-medium-up menutop"></span></a></li>
      <li><a href="index.php?q=salty"><span data-hover="&nbsp;&nbsp;&nbsp;&nbsp;SALTY"><img src="img/icon_tab_salty.png" class="show-for-medium-up menutop"></span></a></li>
      <li><a href="events.php"><span data-hover="&nbsp;&nbsp;&nbsp;EVENTS"><img src="img/icon_tab_events.png" class="show-for-medium-up menutop"></span></a></li>
      <li><a href="index.php?q=contact"><span data-hover="&nbsp;&nbsp;CONTACT"><img src="img/icon_tab_contact.png" class="show-for-medium-up menutop"></span></a></li>
    </ul>
  </section>
</nav>
</div>
</div>