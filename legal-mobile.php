<div class="row">
	<div class="small-12 columns" id='terms'>
		<h3>Terms and Conditions</h3>
		<h3>23.01.2015</h3>
		<p id='legal'>
			
		<strong>Article 1 – Scope</strong><br/><br/>

These terms and conditions are only applicable to products offered for sale bearing the trademarks of the Company Sydney's Home Baked Goods (hereinafter referred to as the Company) entered remotely via the website of the Company identified by domain name: www.sydneyshbg.com (hereinafter referred to as the Site):<br/><br/>

- By taking orders by non-commercial individuals customers who as a consumer and have their habitual residence in the territory defined below,<br/><br/>

- For a delivery to these customers for their own account or to any third party of their choice also habitually resident in the same territory and the quality of the consumer. - To withdrawal at Sydney's Home Baked Goods store located MWEA Tower, Shwedagon Pagoda Road, Yangon, Myanmar for their own account or to any third party of their choice also habitually resident in the same territory and the quality of consumer .<br/><br/>

Any sale of products bearing the trademarks of the Company through the Site is subject to these terms and conditions, which the client accepts. These may be subject to changes and updates, the conditions applicable to the order of an article by a client are those in effect at the date of order.
<br/><br/>
The territory includes the city of Yangon.
<br/><br/>
<strong>Article 2 – Company identification</strong><br/><br/>

Sydney’s Home Baked Goods<br/>
MWEA Tower, Shwedagon Pagoda Road, Yangon, Myanmar<br/><br/>


<strong>Article 3 – Information related to articles and disclaimer</strong><br/><br/>

The Company can not be held responsible for non-substantial errors that could affect the characteristics of the items.
<br/><br/>
However, photographs and other reproductions of articles are indicative and not contractually binding.
<br/><br/>
The Company shall not be liable for any difficulties or damages resulting from the use of the Internet such as interruption of service or customer connection failure to the Site.

<br/><br/>
<strong>Article 4 – Orders terms</strong><br/><br/>

4.1 To order from the website, the customer must be over eighteen, have his usual residence in the territory defined in Article 1 and have the legal capacity. Orders will be accepted within the limits of available stocks.
<br/><br/>
To this end, the customer is informed at the time of ordering, on the information page of each article site describing the availability of the item.
<br/><br/>
Exceptional cases: If an item appears unavailable after placing the order, the Company will notify the customer by phone or email as soon as possible.
<br/><br/>
It is recalled that when ordering, the Company collects personal data. These data are necessary to process the order and will be communicated in whole or part to contractors of the Company acting strictly within the framework of the execution of the order.
<br/><br/>
4.2 The Company shall be entitled, in respect of retail sales, to refuse any order exceeding the allowed number of items.
<br/><br/>
The Company will also be entitled to refuse any order placed by a customer with whom there is a dispute over payment of a previous order not comply with these terms and conditions.
<br/><br/>
If the Company finds that the order does not comply with these terms and conditions it will notify the customer as soon as possible by phone or email.
<br/><br/>
In the event the customer contacts the company to correct the error or contrary to these terms and conditions, the Company reserves the right to cancel the order altogether.

<br/><br/>
<strong>Article 5 – Order through the Site</strong>
<br/><br/>
Taking orders on the Site is subject to strict compliance with procedures materialized by a succession of screens showing the steps the customer must be observed to confirm the order. 
<br/><br/>
After making the order, the department in charge contacts the orders by phone or email to finalize the order.
<br/><br/>

<strong>Article 6 – Order confirmation</strong>
<br/><br/>
E-mails are valid between the parties as well as automatic recording systems used on the Site, such as to the nature and date of the order.
<br/><br/>

<strong>Article 7 - Products prices</strong>
<br/><br/>
The prices displayed on the Site are in Myanmar Kyats (MMK) and include all taxes (commercial tax), excluding shipping charges.
<br/><br/>
Prices charged are those in effect on the date of the order. 
<br/><br/>
The amount of transportation costs associated with the delivery of the items will be sent to the customer when finalizing his order if he chooses to be delivered products. 
<br/><br/>
The customer will receive, upon delivery or passing in store, for each article, written confirmation of the price paid and delivery costs charged to him.
<br/><br/>

<strong>Article 8 - Payment conditions </strong>
<br/><br/>
Accepted payment methods: cash.
<br/><br/>

<strong>Article 9 – Delivery </strong>
<br/><br/>
The goods may occur prior to receipt of payment from the payment of the order, the delivery address given by the customer within the time specified in the order.
<br/><br/>
All delivery passage unsuccessful (wrong address, forgetfulness door code, no stairs, bad name on intercom ...) will be an extension of order processing, and reshipping.
<br/><br/>

<strong>Article 10 – Intellectual Property rights </strong>
<br/><br/>
The site remains the exclusive property of the Company which has created and put online web pages, images and source scripts, database component.
<br/><br/>
The customer is therefore forbidden to distribute or reproduce or link to the Site, in whole or in part, in any form whatsoever.
<br/><br/>

<strong>Article 11 - Droit de rétractation et retour des articles</strong>
<br/><br/>
All orders finalized in contact with the customer service of Sydney's Home Baked Goods can not be canceled and should result in a payment. No refund will be made once the order is confirmed.

<br/><br/>
<strong>Article 12 - Applicable law</strong>
<br/><br/>
These terms and conditions are subject to the Myanmar law.
<br/><br/>
© Sydney's Home Baked Goods 2015

		</p>
	</div>
</div>