

   <div class="row">
    <div class="large-12 columns">
<ul class="example-orbit" data-orbit>
  <li>
    <img src="img/cover_cupcakes_01.jpg" alt="slide 1" />
    <div class="orbit-caption">
    
    </div>
  </li>
 
</ul>
</div>
</div> <!--- Slideshow -->
<div class="row">
  <div class="small-12 medium-6 large-6 columns leftcon">
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/09.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
                      <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Vanilla<span class="right label">1,500 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>
                         Vanilla cake topped with strawberry icing.</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png" class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC1" />
                        <input type="hidden" name="my-item-name" value="Vanilla Cupcake" />
                         <input type="hidden" name="my-item-price" value="1500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form>
                       </div>
                      </div>
                     
                      </div>
                      
                         
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/07.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Lime<span class="right label">1,500 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>
                        Vanilla caked spiked with lime juice and lime zest topped with vanilla icing and candied lime.</span><br /><br />

                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png" class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC2" />
                        <input type="hidden" name="my-item-name" value="Lime Cupcake" />
                         <input type="hidden" name="my-item-price" value="1500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div>
                      </div>
                       
                         </div>
                         
                      
                     </div>
                 </div>
        </div>

        <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/04.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Red Velvet<span class="right label">1,500 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>
                        Red Velvet cake topped with vanilla cream and red fondant heart. Individually gift wrapped. Minimum purchase of 24pcs.</span><br /><br />

                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png" class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC3" />
                        <input type="hidden" name="my-item-name" value="Red Velvet Cupcake" />
                         <input type="hidden" name="my-item-price" value="1500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div>
                      </div>
                       
                         </div>
                         
                      
                     </div>
                 </div>
        </div>


       
        <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/08.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Rainbow<span class="right label">1,500 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Chocolate cake topped with vanilla icing and rainbow sprinkles.</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png" class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC4" />
                        <input type="hidden" name="my-item-name" value="Rainbow Cupcake" />
                         <input type="hidden" name="my-item-price" value="1500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                      
                        </div>
                        
                      
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/03.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Choco Caramel<span class="right label">1,800 MMK</span></span><br /><br />
                       <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape"></span>
                         Chocolate cake topped with caramel cream, caramel and sea salt.</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png" class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC5" />
                        <input type="hidden" name="my-item-name" value="Choc Caramel Cupcake" />
                         <input type="hidden" name="my-item-price" value="1800" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form>
                      </div></div>
                      </div>
                      
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/02.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Passion Fruit<span class="right label">1,700 MMK</span></span><br /><br />
                      <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape"></span>Passion fruit cake spiked with fresh passion fruit juice, passion fruit curd filling topped with passion fruit cream.</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                        <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC6" />
                        <input type="hidden" name="my-item-name" value="Fruit Passion Cupcake" />
                         <input type="hidden" name="my-item-price" value="1700" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                     
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/31.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Mix Box<span class="label right">19,700 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Rainbow, Ganache, Redvelvet, Chocolate, Vanilla, Lime, Choc-Caramel, Passionfruit, Carrot, Chocolate, Choc-peanut butter and Black Sesame Seed</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png" class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC7" />
                        <input type="hidden" name="my-item-name" value="Mix Box" />
                         <input type="hidden" name="my-item-price" value="19700" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div>
                      </div>
                     
                        </div>
                     
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/27.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">I Love You<span class="label right">20,000 MMK</span></span><br ><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Perfect way to say ‘I love You’ with 8 Red Velvet and 4 Ganache cupcakes with red and pink sugar fondant decorations.</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png" class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC8" />
                        <input type="hidden" name="my-item-name" value="I Love You Cupcake" />
                         <input type="hidden" name="my-item-price" value="20000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form>
                        </div>
                      </div>
                    
                     </div>
                      
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/12.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Over Him<span class="label right">20,500 MMK</span></span><br /><br />
                      <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape"></span>Celebrate being single and fabulous with 7 Vanilla and 5 Ganache cupcakes decorated with chocolate colour sugar fondant.</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png" class="plusquantity">
    
                        <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC9" />
                        <input type="hidden" name="my-item-name" value="Over Him Cupcake" />
                         <input type="hidden" name="my-item-price" value="20500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div>
                      </div>
                     
                     
                        </div>
                      
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/13.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Great Job<span class="label right">20,000 MMK</span></span><br /><br />
                      <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape"></span>Ideal gift for graduation, Job promotion or simply just being great at you job! 8 Lime and 4 Ganache decorated with sugar fondant.</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png" class="plusquantity">
    
                        <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC10" />
                        <input type="hidden" name="my-item-name" value="Great Job Cupcake" />
                         <input type="hidden" name="my-item-price" value="20000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div>
                      </div>
                      
                     </div>
                      
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/14.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Get Well<span class="label right">18,000 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Sending your best wishes for a speedy recovery for someone you care with these cupcakes, 7 Vanilla with vanilla icing and 5 Ganache, decorated with fondant sugar.</span><br /><br />
                       <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png" class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC11" />
                        <input type="hidden" name="my-item-name" value="Get Well Cupcake" />
                         <input type="hidden" name="my-item-price" value="18000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div>
                      </div>
                      
                       </div>
                      
                     </div>
                 </div>
        </div>
        <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/15.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Thinking of You<span class="label right">22,500 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Great way to show you are thinking of a love one. 11 Chocolate with vanilla icing and 4 Red Velvet (these cupcakes will be placed in two boxes).</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png" class="plusquantity">
    
                        <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC12" />
                        <input type="hidden" name="my-item-name" value="Thinking of You Cupcake" />
                         <input type="hidden" name="my-item-price" value="22500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div>
                      </div>
                     
                         </div>
                      
                     </div>
                 </div>
        </div> 

  </div>
       <div class="small-12 medium-6 large-6 columns rightcon">
         





        <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/16.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Than MMK A Lot<span class="label right">18,000 MMK</span></span><br /><br />
                      <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape"></span>Show your gratitude with this box. 12 Chocolate with vanilla icing, decorated with sugar fondant.</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC13" />
                        <input type="hidden" name="my-item-name" value="Than MMK A Lot Cupcake" />
                         <input type="hidden" name="my-item-price" value="18000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                     
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/28.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Icons<span class="right label">24,000 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Indulge in these beautiful chocolate cupcakes dipped in chocolate ganache and decorated with sugar fondant is the perfect gift for any occasion.</span><br /><br />
    
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                          <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC14" />
                        <input type="hidden" name="my-item-name" value="Icons" />
                         <input type="hidden" name="my-item-price" value="24000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                      
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/17.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Poker Night<span class="right label">21,000 MMK</span></span><br /><br />
                      <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape"></span>Great way to pair these sweet treats with your friends while playing Poker.  6 Red Velvet and 6 Chocolate Ganache decorated with sugar fondant.</span><br /><br />
    
                      
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                        <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC15" />
                        <input type="hidden" name="my-item-name" value="Poker Night Cupcake" />
                         <input type="hidden" name="my-item-price" value="21000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                    
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/29.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Diva<span class="right label">21,000 MMK</span></span><br /><br />
                      <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape"></span>These vibrantly decorated cupcakes will surely brighten up any occasion. 6 Chocolate Ganache and 6 Red Velvet.</span><br /><br />
    
                     
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                        <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC16" />
                        <input type="hidden" name="my-item-name" value="Diva Cupcake" />
                         <input type="hidden" name="my-item-price" value="21000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                      
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/06.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Chocolate<span class="right label">1,500 MMK</span></span><br /><br />
                      <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape">INGREDIENTS:</span><br />Chocolate cake topped with chocolate icing.</span><br /><br />
                      
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC17" />
                        <input type="hidden" name="my-item-name" value="Chocolate Cupcake" />
                         <input type="hidden" name="my-item-price" value="1500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                   
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/01.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Black Sesame Seed<span class="right label">1,500 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Roasted black sesame seed cake topped with black sesame seed icing.</span><br /><br />
    
                        
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                        <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC18" />
                        <input type="hidden" name="my-item-name" value="Black Sesame Seed Cupcake" />
                         <input type="hidden" name="my-item-price" value="1500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                    
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/05.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Carrot<span class="right label">1,800 MMK</span></span><br /><br />
                      <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape"></span>Carrot cake topped with cream cheese icing (This cake contains walnuts).</span><br /><br />
    
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                          <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC19" />
                        <input type="hidden" name="my-item-name" value="Carrot Cupcake" />
                         <input type="hidden" name="my-item-price" value="1800" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                    
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/10.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Choco Peanut Butter<span class="right label">1,800 MMK</span></span><br /><br />
                      <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape"></span>Chocolate cake topped with peanut butter
                      cream and chocolate ganache. </span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                        <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC20" />
                        <input type="hidden" name="my-item-name" value="Choc Peanut Butter Cupcake" />
                         <input type="hidden" name="my-item-price" value="1800" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                     
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/11.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Choco Ganache<span class="right label">2,000 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Chocolate cake topped with rich 
                      chocolate ganache.</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                        <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC21" />
                        <input type="hidden" name="my-item-name" value="Choc Ganache Cupcake" />
                         <input type="hidden" name="my-item-price" value="2000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                      
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/35.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Wedding<span class="label right">315,000 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>
200 Red Velvet cupcakes, with a two layer square cake topper, decorated with red and white roses. Decorations and set up all inclusive.</span><br /><br />
    
                        
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC22" />
                        <input type="hidden" name="my-item-name" value="Wedding Cupcake" />
                         <input type="hidden" name="my-item-price" value="315000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                     
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/19.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Happy Birthday<span class="right label">18,500 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Chocolate cupcakes with vanilla icing and colourful fondant letters and decorations.</span><br /><br />
    
                       
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC23" />
                        <input type="hidden" name="my-item-name" value="Happy Birthday Cupcake" />
                         <input type="hidden" name="my-item-price" value="18500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                    
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/20.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">MOM<span class="label right">18,000 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>4 Black Sesame Seed, 4 Red Velvet, 4 Vanilla and Chocolate with vanilla icing, decorated with fondant letters and flowers.</span><br /><br />
    
                       
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC24" />
                        <input type="hidden" name="my-item-name" value="MOM Cupcake" />
                         <input type="hidden" name="my-item-price" value="18000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                      
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/36.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">DAD<span class="label right">19,500 MMK</span></span><br /><br />
                      <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape"></span>4 Lime, 4  Ganache, 4 Chocolate and vanilla icing and 4 Red Velvet. Decorated with fondant sugar.</span><br /><br />
    
                      
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                          <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC25" />
                        <input type="hidden" name="my-item-name" value="DAD Cupcake" />
                         <input type="hidden" name="my-item-price" value="19500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                   
                        </div>
                      
                     </div>
                 </div>
        </div>

         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/21.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">2015<span class="right label">20,000 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Celebrate New Years with this box. 8 Vanilla topped with vanilla icing and 4 Ganahe. Decorated with sugar fondant blue stars and numbers.</span><br /><br />
    
                       
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                        <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC26" />
                        <input type="hidden" name="my-item-name" value="2015 Cupcake" />
                         <input type="hidden" name="my-item-price" value="20000 MMK" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                    
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/32.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">JOY<span class="label right">19,500 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>3 Ganache, 3 Red Velvet, 3 Chocolate with chocolate icing and 3 Chocolate with vanilla icing. Decorate with Xmas theme sugar fondant.</span><br /><br />  
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC27" />
                        <input type="hidden" name="my-item-name" value="Joy Cupcake" />
                         <input type="hidden" name="my-item-price" value="19500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                     
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/22.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Happy Thadingyut<span class="label right">22,500 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Celebrate Thadingyut and show your love and respect for your elders (these cupcakes will be placed in two boxes).</span><br /><br />
                       
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC28" />
                        <input type="hidden" name="my-item-name" value="Happy Thadingyut Cupcake" />
                         <input type="hidden" name="my-item-price" value="22500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                    
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/24.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Happy Thingyan<span class="right label">22,500 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Celebrate Thingyan with is box, decorated sugar fondant letters and Thingyan flowers – Golden ‘Padauk’ (these cupcakes will be placed in two boxes).</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC29" />
                        <input type="hidden" name="my-item-name" value="Happy Thingyan Cupcake" />
                         <input type="hidden" name="my-item-price" value="22500" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                    
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/26.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">BFF 4 Ever<span class="right label">20,000 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Show how much you appreciate your best friend! 8 Vanilla cupcakes with vanilla icing and 4 Chocolate Ganache, decorated with sugar fondant.</span><br /><br />
                        
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                         <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC30" />
                        <input type="hidden" name="my-item-name" value="BFF 4 Ever Cupcake" />
                         <input type="hidden" name="my-item-price" value="22000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                   
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/30.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">Good Luck<span class="right label">24,000 MMK</span></span><br /><br />
                      <div class="cartform">
                       <span class="cupcakeingredients"><span class="show-for-landscape"></span>Send out your best wishes along with this box. 12 Chocolate Ganache, decorated with sugar fondant.</span><br /><br />
    
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                          <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC31" />
                        <input type="hidden" name="my-item-name" value="Good Luck Cupcake" />
                         <input type="hidden" name="my-item-price" value="24000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                    
                        </div>
                      
                     </div>
                 </div>
        </div>
         <div class="row">
            <div class="small-2 medium-4 large-4 columns">
               <img src="img/200X200/cupcake/34.jpg" class="sweetypic" />
            </div>
            <div class="small-10 medium-8 large-8 columns cupcakeitemtext">
               <div class="cupcakebackground cupcakeitem">
               <div class="clearfix">
                      <div class="left leftproduct">
                      <span class="productsalty">It's A Girl<span class="right label">18,000 MMK</span></span><br /><br />
                      <div class="cartform">
                      <span class="cupcakeingredients"><span class="show-for-landscape"></span>Send your congrats to the happy couple. 12 Vanilla cupcakes with strawberry and purple icing decorated with sugar fondant letters, stars and edible silver sugar candy.</span><br /><br />
                      <form method="post" action="" class="jcart"><img src="img/cupcakes/icon_less_EB6EA0.png" class="minusquantity" /><input type="text" class="sweety1input" name="my-item-qty" /><img src="img/cupcakes/icon_plus_EB6EA0.png"  class="plusquantity">
    
                        <input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
                         <input type="hidden" name="my-item-id" value="CUC32" />
                        <input type="hidden" name="my-item-name" value="It's a Girl Cupcake" />
                         <input type="hidden" name="my-item-price" value="18000" />
                         <input type="submit" name="my-add-button" value="ADD" />
                         </form></div></div>
                    
                        </div>
                      
                     </div>
                 </div>
        </div>
         

       </div>
</div>




