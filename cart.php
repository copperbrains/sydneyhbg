<?php
include_once('jcart/jcart.php');
require_once 'lib/swift_required.php';
function check_input($data, $problem='')
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    if ($problem && strlen($data) == 0)
    {
        show_error($problem);
    }
    return $data;
}

function redirect($url){
    if (headers_sent()){
        die('<script type="text/javascript">window.location.href="' . $url . '";</script>');
    }else{
        header('Location: ' . $url);
        die();
    }
}
if(isset($_POST['submit']) && strtoupper($_SERVER['REQUEST_METHOD'])=='POST'){

    $url = 'http://sydneyshbg.com/cart.php?error=captcha';
    /* echo '<pre>';
        var_dump($_POST);
      echo '</pre>';*/

    /** retrieve data from session  */

    require_once('recaptchalib.php');
    $privatekey = "6LfQQ-4SAAAAAK6TKOU99rWAmWr1pOwlF6U-r8Qa";
    $resp = recaptcha_check_answer ($privatekey,
        $_SERVER["REMOTE_ADDR"],
        $_POST["recaptcha_challenge_field"],
        $_POST["recaptcha_response_field"]);
    if (!$resp->is_valid) {
        // What happens when the CAPTCHA was entered incorrectly
        redirect($url);
        die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
            "(reCAPTCHA said: " . $resp->error . ")");

    } else {
        $items = $jcart->display_cart_session();


        $contact_name = check_input($_POST['name']);
        $contact_subject = 'Order Form';
        $contact_email = check_input($_POST['emailaddress']);
        $contact_phone = check_input($_POST['phone']);
        $contact_company = check_input($_POST['company']);
        $input_message = check_input($_POST['messages']);


        $total = 0;
        $result = '
     <table style="border-collapse:collapse;border-spacing:0;width:100%">
<tbody><tr><td colspan="4" style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: rgb(216, 215, 215) !important;"><center><img src="https://ci4.googleusercontent.com/proxy/PCpBqYdk2TfRACN1WHFSlwiQvVjbeLsEs4WhXdG8rnFmfYDLC_iZVwEAydT0TUlIiPHyHQ=s0-d-e1-ft#http://sydneyshbg.com/logo.png"></center></td></tr>
<tr>
<td style="font-family:Arial,sans-serif;font-size:14px;color: white;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #8DD5EC;border-color: rgb(216, 215, 215) !important;">Customer Name</td>
<td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border: rgb(216, 215, 215)!important;">' . $contact_name . '</td>
<td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #8DD5EC;border-color: rgb(216, 215, 215) !important;color: white;">Company</td>
<td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: rgb(216, 215, 215) !important;">' . $contact_company . '</td>
</tr>
<tr>
<td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #8DD5EC;color: white;border-color: rgb(216, 215, 215)!important;">Email</td>
<td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: rgb(216, 215, 215) !important;">' . $contact_email . '</td>
<td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #8DD5EC;color: white;border-color: rgb(216, 215, 215) !important;">Phone</td>
<td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: rgb(216, 215, 215) !important;">' . $contact_phone . '</td>
</tr>
<tr>
<td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #8DD5EC;color: white;border-color: rgb(216, 215, 215) !important;">Messages</td>
<td colspan="3" style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: rgb(216, 215, 215) !important;">' . $input_message . '</td>
</tr>
</table>
      <table style="border-collapse:collapse;border-spacing:0;width:100%;margin-top:20px"><tbody><tr><td colspan="4" style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #8DD5EC;color: white;border-color: rgb(216, 215, 215) !important;"><center><span>Order Items</span></center></td></tr>';
        $result .= '<tr><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #EB6EA0;color: white;border-color: rgb(216, 215, 215) !important;">Product Name</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #EB6EA0;color: white;border-color: rgb(216, 215, 215) !important;">Qty</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #EB6EA0;color: white;border-color: rgb(216, 215, 215) !important;">Price</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #EB6EA0;color: white;border-color: rgb(216, 215, 215) !important;">Sub Total</td></tr>';

        foreach($items as $item){
            $result .= '<tr><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;border-color: rgb(216, 215, 215) !important;word-break:normal">' . $item['name'] . '</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: rgb(216, 215, 215) !important;">' . $item['qty'].'</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: rgb(216, 215, 215) !important;">' . $item['price'] . '</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: rgb(216, 215, 215) !important;">' .$item['subtotal'] .  '</td></tr>';
            $total += $item['subtotal'];
        }




        $result.= '<tr><td colspan="3" style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;background: rgb(216, 215, 215);word-break:normal;border-color: rgb(216, 215, 215) !important;">Total</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;background: white;word-break:normal;border-color: rgb(216, 215, 215) !important;"> '  . $total . '</td></tr>';
        $result .= '</table>';
        $contact_message = $result;



        /**Creating for order form reply**/
        $custom_result = '<table style="border-collapse:collapse;border-spacing:0;width:100%;margin-top:20px"><tbody><tr><td colspan="4" style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #8DD5EC;color: white;border-color: rgb(216, 215, 215) !important;"><center><span>Order Items</span></center></td></tr>';
        $custom_result .= '<tr><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #EB6EA0;color: white;border-color: rgb(216, 215, 215) !important;">Product Name</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #EB6EA0;color: white;border-color: rgb(216, 215, 215) !important;">Qty</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #EB6EA0;color: white;border-color: rgb(216, 215, 215) !important;">Price</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;background: #EB6EA0;color: white;border-color: rgb(216, 215, 215) !important;">Sub Total</td></tr>';

        foreach($items as $item){
            $custom_result .= '<tr><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;border-color: rgb(216, 215, 215) !important;word-break:normal">' . $item['name'] . '</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: rgb(216, 215, 215) !important;">' . $item['qty'].'</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: rgb(216, 215, 215) !important;">' . $item['price'] . '</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: rgb(216, 215, 215) !important;">' .$item['subtotal'] .  '</td></tr>';

        }




        $custom_result.= '<tr><td colspan="3" style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;background: rgb(216, 215, 215);word-break:normal;border-color: rgb(216, 215, 215) !important;">Total</td><td style="font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;background: white;word-break:normal;border-color: rgb(216, 215, 215) !important;"> '  . $total . '</td></tr>';
        $custom_result .= '</table>';
        $product= $custom_result;

        $custom_message = "Dear " . $contact_name . ", <br><br>

Thank you for your online order with Sydney's Home Baked Goods.<br><br>

Our team will contact you within 12 hours by phone or email to finalise the order with you. If not, please call us on +951 381 607.
<br><br>
Thank you for your trust, <br><br>

Sydney’s Home Baked Goods team<br><br>
<strong>YOUR ORDER</strong>
<br><br>" . $product . "<br><br>

<strong>YOUR INFORMATIONS</strong>
<br><br>" .
            $contact_name . "<br>" .
            $contact_email . "<br>" .
            $contact_phone . "<br><br>
<strong>PAYMENT</strong><br><br>

Total: " . $total . " MMK<br><br>

You can retain this receipt for your records.<br><br>

Please note the delivery has to be paid by the customer.<br><br>

This is an auto-generated message. Please do not reply to this message by email. Thank you for choosing Sydney’s Home Baked Goods. Join our delicious community on Facebook!";





        // Create the mail transport configuration
        $transport = Swift_MailTransport::newInstance();

        // Create the message
        $message = Swift_Message::newInstance();
        $message->setTo(array(
            'sydneyshbg@gmail.com' => "Sydneys HBG Order"

        ));
        $message->setSubject($contact_subject);
        $message->setBody($contact_message, 'text/html');
        //$message->setFrom($contact_email, $contact_name);
        $message->setFrom(array(
            'sydneyshbg.web@gmail.com' => "Sydneys HBG"
        ));
        // Send the email
        $mailer = Swift_Mailer::newInstance($transport);
        $mailer->send($message);

        echo 'email successfully sent';
        session_destroy();
        header("Location: index.php?q=success");



        // Create the mail transport configuration
        $transport = Swift_MailTransport::newInstance();

        // Create the message
        $message = Swift_Message::newInstance();
        $message->setTo($contact_email);
        $message->setSubject($contact_subject);
        $message->setBody($custom_message, 'text/html');
        //$message->setFrom($contact_email, $contact_name);
        $message->setFrom(array(
            'sydneyshbg.web@gmail.com' => "Sydneys HBG"
        ));
        // Send the email
        $mailer = Swift_Mailer::newInstance($transport);
        $mailer->send($message);
    }
}

?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sydney's Home Baked Goods</title>
    <link rel="shortcut icon" href="img/favicon.png" type="image/png">
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/component.min.css" />
    <script src="js/modernizr.custom.js"></script>
    <script src="js/modernizr.js"></script>
    <script type="text/javascript">
        var RecaptchaOptions = {
            theme : 'clean'
        };
    </script>
</head>
<body>
<div class="container">
    <div class="show-for-medium-up">
        <?php include('menu.php'); ?>
    </div>
    <div class="show-for-small-only">
        <?php include('mobilemenu.php'); ?>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <div class="ordertitle">
                YOUR ORDER
            </div>
        </div>
    </div>

    <div class="row cartdetail">
        <div class="small-12 columns">
            <div id="jcart"><?php

                $jcart->display_cart();?></div>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <div class="ordertitle">
                YOUR INFORMATIONS
            </div>
        </div>
    </div>

    <div class="row">
        <form parsley-validate method="post" action="cart.php">
            <div class="small-12 medium-6 large-6 columns">
                <div class="infoform">

                    <input type="text" placeholder="First Name, Last Name" id="name" name="name" parsley-required="true" class="required"/>
                    <input type="text" placeholder="Company" id="company" name="company" parsley-required="true" class="required"/>
                    <input type="text" placeholder="Email address" id="emailaddress" name="emailaddress" parsley-type="email" parsley-trigger="change" class="required"/>
                    <input type="text" placeholder="Phone number" id="phone" name="phone" parsley-required="true" class="required" />
                    <textarea placeholder="Messages" parsley-required="true" id="messages" name="messages"></textarea>
                    <?php
                    require_once('recaptchalib.php');
                    $publickey = '6LfQQ-4SAAAAAEfd5soV89KF9ff4qdfXJo8hLOsS'; // you got this from the signup page
                    echo recaptcha_get_html($publickey);
                    ?>
                    <div>
                        <?php if($_GET['error']=='captcha'){
                            echo 'Re-captcah is failed. Please try again ';
                        }
                        ?>
                    </div>

                </div>
            </div>
            <div class="small-12 medium-6 large-6 columns">
                <div class="infoform right">
                    <input type="submit" name="submit" value="SEND YOUR ORDER" />

                </div>
            </div>
        </form>
    </div>


    <div class="row">
        <div class="large-12 columns">
            <footer>
                <div class="right footermenudiv">
                    <ul class="inline-list text-right footermenu">
                        <li><a href="index.php">HOME</a></li>
                        <li><a href="index.php?q=cupcakes">CUPCAKES</a></li>
                        <li><a href="index.php?q=sweety">SWEETY</a></li>
                        <li><a href="index.php?q=salty">SALTY</a></li>
                        <li><a href="events.php">EVENTS</a></li>
                        <li><a href="index.php?q=contact">CONTACT</a></li>
                        <li><a href="index.php?q=legal">LEGALS</a></li>
                        <li><a href="index.php?q=credit">CREDITS</a></li>
                        <li><a href="index.php?q=condition">CONDITIONS</a></li>
                    </ul>
                    <span class="text-right copyright"> &copy; Copyright- www.sydneyshbg.com - Proplused by <a href="http://synapseoriginal.com" target="_blank">Synapse Original</a>
    </span>
                </div>
            </footer>
        </div>
    </div>
</div>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/hovercard.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var hoverHTMLDemoBasic = $('.basket').html();


        $("#basket_hover").hovercard({
            detailsHTML: hoverHTMLDemoBasic,
            width: 500,
            background: '#e5e5e5',



        });
    });
</script>
<script src="jcart/js/jcart.js"></script>
<script src="js/foundation.min.js"></script>
<script src="js/foundation/foundation.orbit.js"></script>
<script type="text/javascript" src="js/parsley.js"></script>
<script type="text/javascript">
    $(document).foundation();
</script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        // This button will increment the value

        // This button will decrement the value till 0
        $(".minusbutton").click(function(e) {
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            fieldName = $(this).attr('field');
            // Get its current value
            var currentVal = parseInt($('input[name='+fieldName+']').val());
            // If it isn't undefined or its greater than 0
            if (!isNaN(currentVal) && currentVal > 0) {
                // Decrement one
                $('input[name='+fieldName+']').val(currentVal - 1);
            } else {
                // Otherwise put a 0 there
                $('input[name='+fieldName+']').val(0);
            }
        });
    });
</script>
</body>
</html>
