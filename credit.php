<div class="row textcontainer">
	<div class="small-12 columns">
		<p class="text-center">
			Web Site Developed and Designed by <a href="http://synapseoriginal.com" target="_blank">Synapse Original</a>.
		</p>
		<p class="text-center">
			Pictures by Cremotiv.
		</p>
		<p class="text-center">
			Texts Created by Sydneys Home Baked Goods.
		</p>
		<p class="text-center">
			Publishing Director Nhat Phuong.
		</p>
	</div>
</div>