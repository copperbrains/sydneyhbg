<?php 
include_once('jcart/jcart.php');


 ?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sydney's Home Baked Goods</title>
    <link rel="shortcut icon" href="img/favicon.png" type="image/png">
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/modernizr.js"></script>
<style>
.tumbax-post img{
  padding-left: 20px !important;
  
}
</style>
  </head>
  <body>
  <div class="container">
<div class="show-for-medium-up">
<?php include('menu.php'); ?>
</div>
<div class="show-for-small-only">
<?php include('mobilemenu.php'); ?>
</div>
<div class="row">
      <div class="small-12 medium-12 large-12 columns">

    <div id="tumbax" class="tumb">
    </div>
  </div></div> 



<div class="row">
  <div class="large-12 columns">
     <footer>
       <div class="right footermenudiv">
       <ul class="inline-list text-right footermenu">
       <li><a href="index.php">HOME</a></li>
        <li><a href="index.php?q=cupcakes">CUPCAKES</a></li>
        <li><a href="index.php?q=sweety">SWEETY</a></li>
        <li><a href="index.php?q=salty">SALTY</a></li>
        <li><a href="events.php">EVENTS</a></li>
        <li><a href="index.php?q=contact">CONTACT</a></li>
        <li><a href="index.php?q=legal">LEGALS</a></li>
       <li><a href="index.php?q=credit">CREDITS</a></li>
       </ul>
       <span class="text-right copyright"> &copy; Copyright- www.sydneyshbg.com - Proplused by <a href="http://synapseoriginal.com" target="_blank">Synapse Original</a>
    </span>
    </div>
    </footer>
  </div>
</div>
</div>

   
    <script src="js/jquery.js"></script>
      <script src="js/jquery.js"></script>
  <script src="js/tumbax.js"></script>
  <script type="text/javascript">
  
  //mandatory----------------
  var tumblrBlogLink="http://sydneyshbg.tumblr.com"; 
  var tumblrApiKey="dsxjcMnvFESh0g9uwe5LMjLu0y8xqqlEk4bGQSgJ1VgOTrdVlX";
  
  //optional-----------------
  var tumbaxWidgetWidth = 100; 
  var pathToLoadingImage = './img/loader.gif';
  var tumbaxColumns = 1;


  
  
  function goClicked() {
    $('#tumbax').empty();
    tumblrBlogLink=$('#tumblrBlogLink').val();
    
    prepareTumbax();
  }
  
  </script>
    <script src="jcart/js/jcart.js"></script>
     <script src="js/foundation.min.js"></script>
    <script src="js/foundation/foundation.orbit.js"></script>
    <script type="text/javascript" src="js/parsley.js"></script>
    <script type="text/javascript">
      $(document).foundation();
    </script>
     <script type="text/javascript">
jQuery(document).ready(function(){
    // This button will increment the value

    // This button will decrement the value till 0
    $(".minusbutton").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
});
    </script>
  </body>
</html>
