<?php
include_once('jcart/jcart.php');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>Sydney's Home Baked Goods - Myanmar</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Content-Language" content="en">
    <meta name="Description" content="Sydney's Home Baked Goods is Yangon's premium destination for baked goods. We create sweet bites of goodness from cupcakes to special occasions.">
    <meta name="Keywords" content="Cupcakes, cakes, muffins, brownies, birthday, yangon, burma, myanmar, best, deliver, gift, box, event, bakery, goods, home, sydney's, ">
    <meta name="Subject" content="Cupcake Bakery Myanmar">
    <meta name="Copyright" content="Sydney's Home Baked Goods">
    <meta name="Author" content="Sydney's Home Baked Goods">
    <meta name="Publisher" content="Sydney's Home Baked Goods">
    <meta name="Identifier-Url" content="http://www.sydneyshbg.com/">
    <meta name="Reply-To" content="sydneyshbg@gmail.com">
    <meta name="Revisit-After" content="1 day">
    <meta name="Robots" content="all">
    <meta name="Rating" content="general">
    <meta name="Distribution" content="global">
    <meta name="Geography" content="Yangon, Myanmar">
    <link rel="shortcut icon" href="img/favicon.png" type="image/png">
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/component.min.css" />
    <link rel="stylesheet" type="text/css" href="css/custom.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--    <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>-->
    <!--    <script src="jcart/js/jcart.js?v=0.3"></script>-->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-59222691-1', 'auto');
        ga('send', 'pageview');

    </script>

<!--    <script src="https://www.google.com/recaptcha/api.js"></script>-->
</head>
<body>
<!--  <div id="bgspinner"></div>
<div id="spinner"></div>-->
<div id="mypage">
    <div class="container">
        <div class="show-for-medium-up">

            <div class="row">
                <div class="large-12 columns">
                    <div class="clearfix custop">

                        <a class="right linkedin" href="https://www.linkedin.com/company/sydney%27s-home-baked-goods?trk=biz-companies-cym" target="_blank"><img src="img/icon_linkedin_sydneys.jpg"></a>
                        <!--<a class="right twitter"><img src="img/icon_twitter_sydneys.jpg"></a>-->
                        <a class="right facebook" href="https://www.facebook.com/sydneysyangon" target="_blank"><img src="img/icon_facebook_sydneys.jpg"></a>
                        <a id = "basket_hover" class="showbasket right" href="cart.php"><span>&nbsp;&nbsp;Your order</span><img src="img/icon_basket_sydneys.jpg" id="basketicon"></a>
                        <div class="basket">
                            <div id="jcart">
                                <?php
                                $jcart->display_cart();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="small-12 columns">
                    <nav class="top-bar cl-effect-5" data-topbar>
                        <ul class="title-area">
                            <li class="name">
                                <a href="index.php"><img src="img/logo.png" class="logo"></a>
                            </li>
                            <li class="toggle-topbar menu-icon"><a href="#"><span>&nbsp;</span></a></li>
                        </ul>

                        <section class="top-bar-section">

                            <!-- Left Nav Section -->
                            <ul class="right">
                                <li class="active"><a href="index.php"><span data-hover="&nbsp;&nbsp;&nbsp;&nbsp;HOME"><img src="img/icon_tab_home.png" class="show-for-medium-up menutop"></a></li>
                                <li><a href="index.php?q=cupcakes"><span data-hover="&nbsp;CUPCAKES"><img src="img/icon_tab_cupcakes.png" class="show-for-medium-up menutop"></span></a></li>
                                <li><a href="index.php?q=sweety"><span data-hover="&nbsp;&nbsp;&nbsp;SWEETY"><img src="img/icon_tab_sweety.png" class="show-for-medium-up menutop"></span></a></li>
                                <li><a href="index.php?q=salty"><span data-hover="&nbsp;&nbsp;&nbsp;&nbsp;SALTY"><img src="img/icon_tab_salty.png" class="show-for-medium-up menutop"></span></a></li>
                                <li><a href="events.php"><span data-hover="&nbsp;&nbsp;&nbsp;EVENTS"><img src="img/icon_tab_events.png" class="show-for-medium-up menutop"></span></a></li>
                                <li><a href="index.php?q=contact"><span data-hover="&nbsp;&nbsp;CONTACT"><img src="img/icon_tab_contact.png" class="show-for-medium-up menutop"></span></a></li>
                            </ul>
                        </section>
                    </nav>
                </div>
            </div>
            <?php if($_GET['q']=='cupcakes'){
                include_once('cupcakes.php');}
            else if($_GET['q']=="sweety"){
                include_once('sweety.php');}
            else if($_GET['q']=="salty"){
                include_once('salty.php');}
            else if($_GET['q']=="contact"){
                include_once('contact.php');}
            else if($_GET['q']=="press"){
                include_once('press.php');}
            else if($_GET['q']=="credit"){
                include_once('credit.php');}
            else if($_GET['q']=="legal"){
                include_once('legal.php');}
            else if($_GET['q']=="customize"){
                include_once('customize.php');}
            else if($_GET['q']=="success"){
                include_once('success.php');}
            else if($_GET['q']=="condition"){
                include_once('condition.php');}
            else{
                include_once('home.php');
            }
            ?>

        </div>

        <div class="show-for-small-only">
            <?php include('mobilemenu.php'); ?>
            <?php if($_GET['q']=='cupcakes'){
                include_once('cupcakes-mobile.php');}
            else if($_GET['q']=="sweety"){
                include_once('sweety-mobile.php');}
            else if($_GET['q']=="salty"){
                include_once('salty-mobile.php');}
            else if($_GET['q']=="contact"){
                include_once('contact-mobile.php');}
            else if($_GET['q']=="press"){
                include_once('press-mobile.php');}
            else if($_GET['q']=="credit"){
                include_once('credit-mobile.php');}
            else if($_GET['q']=="legal"){
                include_once('legal-mobile.php');}
            else if($_GET['q']=="condition"){
                include_once('condition-mobile.php');}
            else if($_GET['q']=="success"){
                include_once('success-mobile.php');}
            else{
                include_once('home-mobile.php');
            }
            ?>

        </div>
        <div class="row">
            <div class="large-12 columns">
                <footer>
                    <div class="right footermenudiv">
                        <ul class="inline-list text-right footermenu">
                            <li><a href="index.php">HOME</a></li>
                            <li><a href="index.php?q=cupcakes">CUPCAKES</a></li>
                            <li><a href="index.php?q=sweety">SWEETY</a></li>
                            <li><a href="index.php?q=salty">SALTY</a></li>
                            <li><a href="events.php">EVENTS</a></li>
                            <li><a href="index.php?q=contact">CONTACT</a></li>
                            <li><a href="index.php?q=legal">LEGALS</a></li>
                            <li><a href="index.php?q=credit">CREDITS</a></li>
                            <li><a href="index.php?q=condition">CONDITIONS</a></li>
                        </ul>
                        <span class="text-right copyright"> &copy; Sydney's Home Baked Goods - Propulsed by <a href="http://synapseoriginal.com" target="_blank">Synapse Original</a>
</span>
                    </div>
                </footer>
            </div>
        </div>
    </div>
</div>

<script src="js/modernizr.custom.js"></script>

<script src="js/modernizr.js"></script>

<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9SKQFy3WqKVcy50UVcGxWbXtwaReEmEw"></script>
<script type="text/javascript" src="js/mapmarker.jquery.js"></script>

<script src="js/foundation.min.js"></script>
<script src="js/foundation/foundation.orbit.js"></script>
<script type="text/javascript" src="js/parsley.js"></script>
<script src="js/hovercard.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        /* for map */
        var myMarkers = {"markers": [
                {"latitude": "16.810621", "longitude":"96.151677", "icon": "img/cup.png", "baloon_text": '<h5 style="margin-bottom:-10px;">International School Yangon (ISY)</h5><br> <img src="img/home.png" style="margin-right:10px;">No(20), Shwe Taungyar Street, Bahan Township,Yangon.<br><br><img src=img/phone.png style="margin-right:10px;"> 01 512793 ~ 5'},
                {"latitude": "16.829887", "longitude":"96.157187", "icon": "img/cup.png", "baloon_text": '<h5 style="margin-bottom:-10px;">Myanmar International School (MIS)</h5></h5><br> <img src="img/home.png" style="margin-right:10px;"> No(20), Pyin Nya Wadi Street, (5) Wards, Yankin Township, Yangon.<br><br><img src=img/phone.png style="margin-right:10px;"> 01 558114 ~ 8'},
                {"latitude": "16.817138", "longitude":"96.130987", "icon": "img/cup.png", "baloon_text": '<h5 style="margin-bottom:-10px;">Scoop Ice-Cream Shop at Junction Square</h5><br> <img src="img/home.png" style="margin-right:10px;"> No(223/224/241), 2nd Floor, Junction Square Centre, Kamayut Township, Yangon.<br><br><img src=img/phone.png style="margin-right:10px;"> 09 31060561'},
                {"latitude": "16.827561", "longitude":"96.139683", "icon": "img/cup.png", "baloon_text": '<h5 style="margin-bottom:-10px;">Techno Land Restaurant </h5><br> <img src="img/home.png" style="margin-right:10px;">No(98),Inya Road,(9) Wards, Kamayut Township, Yangon. <br><br><img src=img/phone.png style="margin-right:10px;"> 01 514303, 09 49280564'},
                {"latitude": "16.774805", "longitude":"96.143899", "icon": "img/cup.png", "baloon_text": '<h5 style="margin-bottom:-10px;">Coffee Club</h5><br> <img src="img/home.png" style="margin-right:10px;">Corner of 11 street and Maharbandula Street, Lanmadaw Township, Yangon. <br><br><img src=img/phone.png style="margin-right:10px;"> 09 43207764'},
                {"latitude": "16.782242", "longitude":"96.153715", "icon": "img/cup.png", "baloon_text": '<h5 style="margin-bottom:-10px;">MWEA Tower</h5><br> <img src="img/home.png" style="margin-right:10px;">#1-06, 288/290, Corner of Yaw Min Gyi Road & Shwedagon Pagoda Road, Yangon. <br><br><img src=img/phone.png style="margin-right:10px;"> 09 5156861, 01-381607'}

            ]
        };

        //set up map options
        $("#map").mapmarker({
            zoom  : 12,
            center  : 'Yangon',
            markers : myMarkers
        });
        $("#map2").mapmarker({
            zoom  : 12,
            center  : 'Yangon',
            markers : myMarkers
        });
        var hoverHTMLDemoBasic = $('.basket').html();


        $("#basket_hover").hovercard({
            detailsHTML: hoverHTMLDemoBasic,
            width: 350,
            background: '#d8d7d7',



        });


    });
</script>


<script type="text/javascript">
    $(document).foundation();
</script>
<script type="text/javascript">
    $(document).ready(function(){


        $('.plusquantity').on('click',function(){

            var input = $(this).prev();

            i = ($(input).val() == '') ? 0 : parseInt($(input).val()) ;
            i = i+1;
            $(input).val('');
            $(input).val(i);

        });
        $('.minusquantity').on('click',function(){

            var input = $(this).next();
            i = ($(input).val() == '' ) ? 0 : parseInt($(input).val()) ;
            if (i != 0 ){
                i = i-1;
                $(input).val('');
                $(input).val(i);
            }
            else{
                $(input).val('0');
            }

        });

        var path = 'jcart',
            container = $('#jcart'),
            token = $('[name=jcartToken]').val(),
            tip = $('#jcart-tooltip');

        var config = (function() {
            var config = null;
            $.ajax({
                url: path + '/config-loader.php',
                data: {
                    "ajax": "true"
                },
                dataType: 'json',
                async: false,
                success: function(response) {
                    config = response;
                },
                error: function() {
                    alert('Ajax error: Edit the path in jcart.js to fix.');
                }
            });
            return config;
        }());

        var setup = (function() {
            if(config.tooltip === true) {
                tip.text(config.text.itemAdded);

                // Tooltip is added to the DOM on mouseenter, but displayed only after a successful Ajax request
                $('.jcart [type=submit]').mouseenter(
                    function(e) {
                        var x = e.pageY + 25,
                            y = e.pageX + -10;
                        $('body').append(tip);
                        tip.css({top: y + 'px', left: x + 'px'});
                    }
                )
                    .mousemove(
                        function(e) {
                            var y = e.pageY + 25,
                                x = e.pageX + -10;
                            tip.css({top: y + 'px', left: x + 'px'});
                        }
                    )
                    .mouseleave(
                        function() {
                            tip.hide();
                        }
                    );
            }

            // Remove the update and empty buttons since they're only used when javascript is disabled
            $('#jcart-buttons').remove();

            // Default settings for Ajax requests
            $.ajaxSetup({
                type: 'POST',
                url: path + '/relay.php',
                success: function(response) {
                    // Refresh the cart display after a successful Ajax request
                    $('#jcart').html(response);
                    $('#jcart-buttons').remove();
                },
                // See: http://www.maheshchari.com/jquery-ajax-error-handling/
                error: function(x, e) {
                    var s = x.status,
                        m = 'Ajax error: ' ;
                    if (s === 0) {
                        m += 'Check your network connection.';
                    }
                    if (s === 404 || s === 500) {
                        m += s;
                    }
                    if (e === 'parsererror' || e === 'timeout') {
                        m += e;
                    }
                    alert(m);
                }
            });
        }());

        // Check hidden input value
        // Sent via Ajax request to jcart.php which decides whether to display the cart checkout button or the PayPal checkout button based on its value
        // We normally check against request uri but Ajax update sets value to relay.php

        // If this is not the checkout the hidden input doesn't exist and no value is set
        var isCheckout = $('#jcart-is-checkout').val();

        function add(form) {
            // Input values for use in Ajax post
            var itemQty = form.find('[name=' + config.item.qty + ']'),
                itemAdd = form.find('[name=' + config.item.add + ']');

            // Add the item and refresh cart display
            $.ajax({
                data: form.serialize() + '&' + config.item.add + '=' + itemAdd.val(),
                success: function(response) {
                    console.log(response);
                    // Momentarily display tooltip over the add-to-cart button
                    if (itemQty.val() > 0 && tip.css('display') === 'none') {
                        tip.fadeIn('100').delay('400').fadeOut('100');
                    }

                    $('#jcart').html(response);
                    $('#jcart-buttons').remove();
                }
            });
        }

        function update(input) {
            // The id of the item to update
            var updateId = input.parent().find('[name="jcartItemId[]"]').val();

            // The new quantity
            var newQty = input.val();

            // As long as the visitor has entered a quantity
            if (newQty) {

                // Update the cart one second after keyup
                var updateTimer = window.setTimeout(function() {

                    // Update the item and refresh cart display
                    $.ajax({
                        data: {
                            "jcartUpdate": 1, // Only the name in this pair is used in jcart.php, but IE chokes on empty values
                            "itemId": updateId,
                            "itemQty": newQty,
                            "jcartIsCheckout": isCheckout,
                            "jcartToken": token
                        }
                    });
                }, 1000);
            }

            // If the visitor presses another key before the timer has expired, clear the timer and start over
            // If the timer expires before the visitor presses another key, update the item
            input.keydown(function(e){
                if (e.which !== 9) {

                    window.clearTimeout(updateTimer);
                }
            });


        }

        function remove(link) {
            // Get the query string of the link that was clicked
            var queryString = link.attr('href');
            queryString = queryString.split('=');

            // The id of the item to remove
            var removeId = queryString[1];

            // Remove the item and refresh cart display
            $.ajax({
                type: 'GET',
                data: {
                    "jcartRemove": removeId,
                    "jcartIsCheckout": isCheckout
                }
            });
        }

        // Add an item to the cart
        $('.jcart').submit(function(e) {
            add($(this));
            e.preventDefault();
        });

        // Prevent enter key from submitting the cart
        $('#jcart').keydown(function(e) {
            if(e.which === 13) {
                e.preventDefault();
            }
        });

        // Update an item in the cart
        $('#jcart').on('keyup', '[name="jcartItemQty[]"]', function(){

            update($(this));
        });

        $('#jcart').on('click', '.plusbutton',function(){

            var input = $(this).prev();

            i = parseInt($(input).val());
            i = i+1;
            $(input).val('');
            $(input).val(i);
            update(input);
        });

        $( "#jcart" ).on( "click", ".minusbutton", function() {
            var input = $(this).next();

            i = parseInt($(input).val());
            i = i-1;
            $(input).val('');
            $(input).val(i);
            update(input);
        });



        // Remove an item from the cart
        $('#jcart').on('.jcart-remove', 'click', function(e){
            remove($(this));
            e.preventDefault();
        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){



        $("a.grouped_elements").fancybox({
            'transitionIn'  :   'ease',
            'transitionOut' :   'ease',
            'speedIn'       :   500,
            'speedOut'      :   200,
            'overlayShow'   :   false
        });



    });

</script>
<script>
    $(document).ready(function() {
        $('.fancybox').fancybox();

    });

    $(".pressv").click(function() {
        $.fancybox({
            'padding'   : 0,
            'autoScale'   : false,
            'transitionIn'  : 'none',
            'transitionOut' : 'none',
            'title'     : this.title,
            'width'   : 680,
            'height'    : 495,
            'href'      : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type'      : 'swf',
            'swf'     : {
                'wmode'    : 'transparent',
                'allowfullscreen' : 'true'
            }
        });

        return false;
    });
</script>

<script type="text/javascript">
    $("input[name='my-add-button']").click(function(){
        let qty = $("input[name=my-item-qty]").val();
        if(qty == null || qty > 0 ){
            if(qty == 1){
                alert('You have added '+ qty +' item.')
            }else{
                alert('You have added '+ qty +' items.')
            }
        }else{
            alert('Please select the item.')
        }
    });

</script>
</body>
</html>
