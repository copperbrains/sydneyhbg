<?php 
include_once('jcart/jcart.php');
require_once 'lib/swift_required.php';
function check_input($data, $problem='')
  {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    if ($problem && strlen($data) == 0)
    {
      show_error($problem);
    }
    return $data;
  }
if(isset($_POST['submit']) && strtoupper($_SERVER['REQUEST_METHOD'])=='POST'){
   
   /* echo '<pre>';
      var_dump($_POST);
    echo '</pre>';*/

    /** retrieve data from session  */
     $items = $jcart->display_cart_session(); 
     

     $contact_name = check_input($_POST['name']);
    $contact_subject = 'Order Form';
    $contact_email = check_input($_POST['emailaddress']);
    $contact_phone = check_input($_POST['phone']);
    $contact_company = check_input($_POST['company']);
    $input_message = check_input($_POST['messages']);


      $total = 0;
      $result = '
      <table style="border-collapse: collapse;
border-spacing: 0;
width: 100%;" >
<tr><td colspan="4" style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;"><center><img src="http://sydneyshbg.com/logo.png" /></center></td></tr>
<tr>
<td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">Customer Name</td>
<td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">' . $contact_name . '</td>
<td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">Company</td>
<td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">' . $contact_company . '</td>
</tr>
<tr>
<td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">Email</td>
<td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">' . $contact_email . '</td>
<td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">Phone</td>
<td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">' . $contact_phone . '</td>
</tr>
<tr>
<td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">Messages</td>
<td colspan="3" style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">' . $input_message . '</td>
</tr>
</table>
      <table style="border-collapse: collapse;
border-spacing: 0;
width: 100%; margin-top: 20px;" ><tr><td colspan="4" style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;"><center><span>Order Items</span></center></td></tr>';
      $result .= '<tr><td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">Product Name</td><td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">Qty</td><td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">Price</td><td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">Sub Total</td></tr>';
      
      foreach($items as $item){
        $result .= '<tr><td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">' . $item['name'] . '</td><td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">' . $item['qty'].'</td><td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">' . $item['price'] . '</td><td style="font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;">' .$item['subtotal'] .  '</td></tr>';
        $total += $item['subtotal'];
      }
      $result.="<tr><td colspan='3' style='font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;''>Total</td><td style='font-family: Arial, sans-serif;
font-size: 14px;
padding: 10px 5px;
border-style: solid;
border-width: 1px;
overflow: hidden;
word-break: normal;''>$total</td></tr>";
      $result .= '</table>';
      $contact_message = $result;

    
    
    


    // Create the mail transport configuration
    $transport = Swift_MailTransport::newInstance();
     
    // Create the message
    $message = Swift_Message::newInstance();
    $message->setTo(array(
     'suthaw524@gmail.com' => "Thiha Tin Maung Aye"
    ));
    $message->setSubject($contact_subject);
    $message->setBody($contact_message, 'text/html');
    $message->setFrom($contact_email, $contact_name);
     
    // Send the email
    $mailer = Swift_Mailer::newInstance($transport);
    $mailer->send($message);

    echo 'email successfully sent';

  }

 ?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sydney's Home Baked Goods</title>
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/modernizr.js"></script>

  </head>
  <body>
<div class="show-for-medium-up">
<?php include('menu.php'); ?>
</div>
<div class="show-for-small-only">
<?php include('mobilemenu.php'); ?>
</div>
<div class="row">
<div class="small-12 columns">
  <div class="ordertitle">
    YOUR ORDER
  </div>
</div>
</div>

<div class="row cartdetail">
<div class="small-12 columns">
      <div id="jcart"><?php 

$jcart->display_cart();?></div>
     </div> 
 </div>
 
<div class="row">
<div class="small-12 columns">
  <div class="ordertitle">
    YOUR INFORMATIONS
  </div>
</div>
</div>

<div class="row">
<form parsley-validate method="post" action="cart.php">
<div class="small-12 medium-6 large-6 columns">
  <div class="infoform">
    
      <input type="text" placeholder="First Name, Last Name" id="name" name="name" parsley-required="true" class="required"/>
      <input type="text" placeholder="Company" id="company" name="company" parsley-required="true" class="required"/>
      <input type="text" placeholder="Email address" id="emailaddress" name="emailaddress" parsley-type="email" parsley-trigger="change" class="required"/>
      <input type="text" placeholder="Phone number" id="phone" name="phone" parsley-required="true" class="required" />
      <textarea placeholder="Messages" parsley-required="true" id="messages" name="messages"></textarea>
      
    
  </div>
</div>
<div class="small-12 medium-6 large-6 columns">
<div class="infoform right">
<input type="submit" name="submit" value="SEND YOUR ORDER" />
</div>
</div>
</form>
</div>


<div class="row">
  <div class="large-12 columns">
    <footer>
       &nbsp;
    </footer>
  </div>
</div>


   
    <script src="js/jquery.js"></script>
    <script src="js/hovercard.js"></script>
    <script type="text/javascript">
      $(document).ready(function () {

    var hoverHTMLDemoBasic = $('.basket').html();
        

    $("#basket_hover").hovercard({
        detailsHTML: hoverHTMLDemoBasic,
        width: 500,
        background: '#e5e5e5',


       
    });
});
    </script>
    <script src="jcart/js/jcart.js"></script>
     <script src="js/foundation.min.js"></script>
    <script src="js/foundation/foundation.orbit.js"></script>
    <script type="text/javascript" src="js/parsley.js"></script>
    <script type="text/javascript">
      $(document).foundation();
    </script>
     <script type="text/javascript">
jQuery(document).ready(function(){
    // This button will increment the value

    // This button will decrement the value till 0
    $(".minusbutton").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
});
    </script>
  </body>
</html>