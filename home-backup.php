<div class="row">
  <div class="small-12 columns">
    <ul class="example-orbit" data-orbit>
        <li>
          <img src="img/home_slider_img1.jpg" alt="slide 1" />
          <div class="orbit-caption">
          </div>
        </li>
        <li>
          <img src="img/home_slider_img2.jpg" alt="slide 2" />
          <div class="orbit-caption">
          </div>
        </li>
        <li>
          <img src="img/home_slider_img3.jpg" alt="slide 3" />
          <div class="orbit-caption">
          </div>
        </li>
    </ul>
  </div>
</div><!-- Slideshow -->
<div class="row">
  <div class="small-12 medium-6 large-6 columns columnswithnopaddingright">
  <div class="wheredo">
            <div class"moduletitle" style="padding-top: 20px; padding-left: 20px;">
            <span>WHERE DO WE FIND US?</span>
          </div>
          <a href="index.php?q=contact" class="button postfix" style="width: 150px; float: right; margin-right: 20px;">Find a Shop</a>
   </div>
    <div class="row">
      <div class="small-6 medium-6 large-6 columns">
      <div class="journal">
       <div class"moduletitle" style="padding-top: 30px;"><span class="journaltext">WHAT THEY SAY ABOUT US...</span></div>
       <a href="#" class="button postfix">Go</a>
      </div>
        
      </div><!-- What they say about us -->
      <div class="small-6 medium-6 large-6 columns">
       <img src="img/press_img1.jpg">
      </div><!-- Megazine pictures -->
    </div><!-- megazine row -->
  </div><!-- left side main -->
  <div class="small-12 medium-6 large-6 columns columnswithnopaddingleft">
    <div class="facebooksocial">
        <div class"moduletitle" style="padding-top: 20px; padding-left: 20px;">
          <span class="socialfirst">JOIN THE</span><span class="socialsecond"> SYDNEY'S<br /> COMMUNITY</span><span class="socialfirst"> ON FACEBOOK</span>
        </div>
        <img src="img/facebook_timeline_img.jpg" class="fbpic">
      </div>
  </div><!-- right side Facebook Page -->
  </div>
   